import { combineReducers } from 'redux';
import auth from './auth/reducers';
import user from './user/reducers'

const rootReducer = combineReducers({
    auth,
    user,
});

export default rootReducer;