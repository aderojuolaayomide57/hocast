export const BASE_URL = 'https://hocast-backend.herokuapp.com/api/';
export const LOGIN_URL = `${BASE_URL}auth`;
export const REGISTER_URL = `${BASE_URL}users/`;
export const CREATE_PROFILE_URL =`${BASE_URL}profile/`;
export const GET_PROFILE_URL = `${BASE_URL}profile/me`;
