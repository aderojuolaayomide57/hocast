import { axiosInstance } from './axiosDefaults';

export const apiRequest = (method, url, data = {}, token) => {
  const response = axiosInstance(token)({
    method,
    url,
    data,
    token,
  });
  return response;
};