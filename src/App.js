import React, { Fragment, useEffect } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';

import { Provider } from 'react-redux';
import store from './services/redux/store';
import { persistor } from './services/redux/store';
import { PersistGate } from 'redux-persist/integration/react';
import 'react-toastify/dist/ReactToastify.min.css';
import Login from './screens/Auth/Login';
import Signup from './screens/Auth/Signup';
import UserProfile from './screens/Dashboard';
import Profile from './screens/Profile';
import Event from './screens/Event';
import WaitingRoom from './screens/LiveEvent/WaitingRoom';
import LiveStudio from './screens/LiveEvent/LiveStudio/';
import VisitorSignUp from './screens/VisitorAuth/UserSignup';
import VisitorLogin from './screens/VisitorAuth/UserLogin';
import EventPageView from './screens/EventPageView';
import Payment from './screens/PaymentUpdate/';
import ManageEvent from './screens/ManageEvent'
import test from './screens/test';



import { Creators } from '../src/services/redux/user/actions';



const App = () => {
  useEffect(() => {
    store.dispatch(Creators.getUserDetails());
  }, []);
  
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Router forceRefresh={true}>
          <Fragment>
            <ToastContainer
              position='top-right'
              autoClose={100}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnVisibilityChange
              draggable
              pauseOnHover
            />
            <ToastContainer />
            
            <Switch>
              <Route path='/' exact component={Login} />
              <Route path='/signup' exact component={Signup} />
              <Route path='/profile' exact component={Profile} />
              <Route path='/dashboard' exact component={UserProfile}/>
              <Route path='/createevent' exact component={Event}/>
              <Route path='/waitingroom' exact component={WaitingRoom}/>
              <Route path='/livestudio' exact component={LiveStudio}/>
              <Route path='/visitor' exact component={VisitorSignUp}/>
              <Route path='/login' exact component={VisitorLogin}/>
              <Route path='/viewevent' exact component={EventPageView}/>
              <Route path='/payment' exact component={Payment}/>
              <Route path='/manageevent' exact component={ManageEvent}/>
            </Switch>
          </Fragment>
        </Router>
      </PersistGate>
    </Provider>
  );
};

export default App;
