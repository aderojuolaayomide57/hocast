import React, { Component } from 'react';
import Header from '../Dashboard/Header';
import { Link } from 'react-router-dom';
import ErrorMessage from '../../components/ErrorMessage/errorMessage';
import CustomButton from '../../components/Button';
import CustomTitleText from '../../components/Title';
import CustomLabelText from '../../components/Label';


import './style.css';



class ManageEvent extends Component{

   constructor(props) {
       super(props);
       this.state = {
         
       }
   }
        
     
   render() {
      return (
         <div className='managepage'>
            <Header/> 
            <div className="container" style={styles.topContainer}>
                <div className='container' style={styles.middleContainer}>
                    <CustomTitleText
                        text="Manage your events"
                        size="sm"
                        style={styles.customTitle}
                    />
                    <table className="eventtable">
                        <tr>
                            <th>
                                <label class="checkwrapper">
                                    <input type="checkbox" />
                                    <span class="checkmark"></span>
                                </label>
                            </th>
                            <th>Event Image</th>
                            <th>Event Title</th>
                            <th>Category</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Session</th>
                            <th>Price</th>
                            <th></th>
                        </tr>
                        <tr>
                            <td>
                                <label class="checkwrapper">
                                    <input type="checkbox" />
                                    <span class="checkmark"></span>
                                </label>
                            </td>
                            <td>Price</td>
                            <td>Tech & Post COVID-19</td>
                            <td>Education</td>
                            <td>23 April 2020</td>
                            <td>03:00PM</td>
                            <td>2</td>
                            <td>₦ 3,500.00</td>
                            <td><i class="fa fa-ellipsis-v" aria-hidden="true"></i></td>
                        </tr>
                        <tr>
                            <td>
                                <label class="checkwrapper">
                                    <input type="checkbox" />
                                    <span class="checkmark"></span>
                                </label>
                            </td>
                            <td>Price</td>
                            <td>Tech & Post COVID-19</td>
                            <td>Education</td>
                            <td>23 April 2020</td>
                            <td>03:00PM</td>
                            <td>2</td>
                            <td>₦ 3,500.00</td>
                            <td><i class="fa fa-ellipsis-v" aria-hidden="true"></i></td>
                        </tr>
                        <tr>
                            <td>
                                <label class="checkwrapper">
                                    <input type="checkbox" />
                                    <span class="checkmark"></span>
                                </label>
                            </td>
                            <td>Price</td>
                            <td>Tech & Post COVID-19</td>
                            <td>Education</td>
                            <td>23 April 2020</td>
                            <td>03:00PM</td>
                            <td>2</td>
                            <td>₦ 3,500.00</td>
                            <td><i class="fa fa-ellipsis-v" aria-hidden="true"></i></td>
                        </tr>
                        <tr>
                            <td>
                                <label class="checkwrapper">
                                    <input type="checkbox" />
                                    <span class="checkmark"></span>
                                </label>
                            </td>
                            <td>Price</td>
                            <td>Tech & Post COVID-19</td>
                            <td>Education</td>
                            <td>23 April 2020</td>
                            <td>03:00PM</td>
                            <td>2</td>
                            <td>₦ 3,500.00</td>
                            <td><i class="fa fa-ellipsis-v" aria-hidden="true"></i></td>
                        </tr>
                        <tr>
                            <td>
                                <label class="checkwrapper">
                                    <input type="checkbox" />
                                    <span class="checkmark"></span>
                                </label>
                            </td>
                            <td>Price</td>
                            <td>Tech & Post COVID-19</td>
                            <td>Education</td>
                            <td>23 April 2020</td>
                            <td>03:00PM</td>
                            <td>2</td>
                            <td>₦ 3,500.00</td>
                            <td><i class="fa fa-ellipsis-v" aria-hidden="true"></i></td>
                        </tr>
                        <tr>
                            <td>
                                <label class="checkwrapper">
                                    <input type="checkbox" />
                                    <span class="checkmark"></span>
                                </label>
                            </td>
                            <td>Price</td>
                            <td>Tech & Post COVID-19</td>
                            <td>Education</td>
                            <td>23 April 2020</td>
                            <td>03:00PM</td>
                            <td>2</td>
                            <td>₦ 3,500.00</td>
                            <td><i class="fa fa-ellipsis-v" aria-hidden="true"></i></td>
                        </tr>
                    </table>
                </div>
            </div>
            
         </div>
      );
   }   
};

const styles = {
    topContainer: {
        paddingTop: 40,
        paddingBottom: 50,
        overflowX: 'auto',
    },
    middleContainer: {
        padding: 25,
        backgroundColor: 'white',
        overflowX: 'auto',
    },
    customTitle: {
        marginTop: 0,
        paddingBottom: 10, 
        fontSize: 18,
        paddingLeft: 10,
    } 
}

export default ManageEvent;