import React, { Fragment,Component } from 'react';
import { Link, useHistory, useLocation } from 'react-router-dom';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { Creators } from '../../services/redux/user/actions';
import ProfileAbout from './ProfileAbout';
import BusinessProfile from './BusinessProfile';
import Plans from './Plan';
import CongratMessage from './CongratMessage';
import ProgressBar from '../../components/ProgressBar';
import PaymentModal from '../Payment';
import CustomLoader from '../../components/Loader';
import store from '../../services/redux/store';





import './style.css';

class Profile extends Component{

    constructor(props) {
        super(props);
        this.selectPage = this.selectPage.bind(this);
        this.getProfileData = this.getProfileData.bind(this);
        this.state = {
            //selectedPage: 'plans',
            selectedPage: localStorage.getItem('profile_page') ? localStorage.getItem('profile_page') : 'about',
            values: [],
            loading: false
        }
    }

    componentDidMount() {
      setTimeout(function() { //Start the timer
          this.setState({loading: true}) //After 1 second, set render to true
      }.bind(this), 1100)
    }

    

    componentWillMount() {
      if(store.getState().auth.token){
        this.props.getUserProfile();
      }else{
        this.props.history.push(`/`)
      }
    }

    selectPage = selected => {
        this.setState({ selectedPage: selected});
        localStorage.setItem('profile_page', selected)
    }

    getProfileData = data => {
      this.setState({ values: data});
      console.log(this.state.values);
    }

    render(){
        const { user } = this.props;
          return (
              <Fragment>
                <div className='_body'>
                  <section className='_box-container'>
                    <div className='img-box'>
                      
                      <img
                        src={process.env.PUBLIC_URL + '../../assets/image/Group 2895.svg'}
                        className='profile_logo'
                        alt='Logo'
                      />
                    </div>
                    <ProgressBar/>
                    { !this.state.loading && 
                      <CustomLoader/>
                    }
                    
                    { (this.state.selectedPage === 'about' && this.state.loading) && 
                    <ProfileAbout
                          {...this.props}
                          isCharging={this.state.isCharging}
                          getProfileData={(data) => {
                            this.getProfileData(data)
                            this.selectPage('business')
                          }}
                    /> }
                    { (this.state.selectedPage === 'business' && this.state.loading) && 
                    <BusinessProfile
                          {...this.props}
                          isCharging={this.state.isCharging}
                          aboutData = {this.state.values}
                          onSubmit={(data) => {
                            this.selectPage('plans')
                            this.props.createUserProfile(data)
                          }}
                    /> }
                    { (this.state.selectedPage === 'plans' && this.state.loading) && 
                    <Plans
                          {...this.props}
                          isCharging={this.state.isCharging}
                          //selectPage={() => this.selectPage('business')}
                          onSubmit={(values) => this.selectPage('congrats')}
                    /> }
                    { (this.state.selectedPage === 'congrats' && this.state.loading) && 
                      <CongratMessage />
                    }
                    
                  </section>
                </div>
              </Fragment>
          )
        
    }
 
};
Profile.protoTypes = {
  user: PropTypes.object.isRequired,
  error: PropTypes.string,
};

const mapDispatchToProps = dispatch => {
  return {
    createUserProfile: data => {
      dispatch(Creators.createUserProfileRequest(data))
    },
    getUserProfile: () => {
      dispatch(Creators.getUserDetails())
    }
  }
}

const mapStateToProps = (state) => ({
  user: state.user.user,
  error: state.user.error_message,
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);

