import React, { Fragment } from 'react';
import { Link, useHistory, useLocation } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';
import ErrorMessage from '../../../components/ErrorMessage/errorMessage';
import CustomTextInput from '../../../components/TextInput';
import CustomButton from '../../../components/Button';
import CustomTitleText from '../../../components/Title';
import CustomLabelText from '../../../components/Label';




const validationSchema = Yup.object().shape({
        businessname: Yup.string()
        .label('Business Name')
        .required('Business Name is required'),
        location: Yup.string()
        .label('Business Location')
        .required('Business Name is required'),
})

const BusinessProfile = (props) => {

  const initialValues = {
    title: props.aboutData.title,
    bio: props.aboutData.bio,
    businessname: "",
    location: props.location ? props.user.location : ""
  }

  return (
    <Fragment>
      
      <Formik
            initialValues={initialValues}
            onSubmit={values => props.onSubmit(values)}
            validationSchema={validationSchema}
        >
            {formikProps => (
                <form onSubmit={formikProps.handleSubmit}>
                  <div className='profile_title'>
                        <CustomTitleText
                          text="Tell us about your business"
                          size="sm"
                          style={{textAlign: 'center',marginTop: 30,marginBottom: 20,fontSize: 25}}
                        />
                  </div>
                  <div className='_profile_box' style={{marginTop: 20}}>
                    <div className='profile_label'>
                      <CustomLabelText forlabel="businessname" text="What is the name of your business? " style={{fontSize: 15, fontWeight: 'bold'}}/>
                      <br />
                      <CustomTextInput
                        type='text'
                        name='businessname'
                        placeholder='Enter your business name'
                        onChange={formikProps.handleChange}
                        value={formikProps.values.businessname}
                        style={{width: 350}}
                      />
                      <ErrorMessage errorValue={formikProps.errors.businessname}/>
                    </div>
                    <div className='text_label' style={{marginTop: 10}}>
                      <CustomLabelText forlabel="location" text="Where is it located?" style={{fontSize: 15, fontWeight: 'bold'}}/>
                      <br />
                      <CustomTextInput
                        type='text'
                        name='location'
                        placeholder='Enter business location'
                        onChange={formikProps.handleChange}
                        value={formikProps.values.location}
                        style={{width: 350}}
                      />
                      <ErrorMessage errorValue={formikProps.errors.location}/>
                    </div>
                    <br/>
                    <CustomButton 
                      type="submit"
                      value="Next"  
                      style={{ width: 100,padding: 8,marginLeft: '66%',marginTop: 25 }}  
                    />
                    
                  </div>
                </form>
            )}
        </Formik>
    </Fragment>
  );
};

export default BusinessProfile;





