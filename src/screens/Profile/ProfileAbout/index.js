import React, { Fragment } from 'react';
import { Link, useHistory, useLocation } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';
import ErrorMessage from '../../../components/ErrorMessage/errorMessage';
import CustomTextInput from '../../../components/TextInput';
import CustomButton from '../../../components/Button';
import CustomTitleText from '../../../components/Title';
import CustomTextArea from '../../../components/TextArea';
import CustomLabelText from '../../../components/Label';




const validationSchema = Yup.object().shape({
        title: Yup.string()
        .label('Title')
        .required('Title is required'),
        bio: Yup.string()
        .label('Bio')
        .required('Bio is required'),
})

const ProfileAbout = (props) => {

  const initialValues = {
    title: props.user ? props.user.title : "",
    bio: props.user ? props.user.bio : ""
  }
  console.log('hello');
  console.log(props.user)

  return (
    <Fragment>
      
      <Formik
            initialValues={initialValues}
            onSubmit={values => props.getProfileData(values)}
            validationSchema={validationSchema}
        >
            {formikProps => (
                <form onSubmit={formikProps.handleSubmit}>
                  <div className='profile_title'>
                        <CustomTitleText
                          text="Tell us about yourself"
                          size="sm"
                          style={{textAlign: 'center',marginTop: 20,marginBottom: 20,fontSize: 25}}
                        />
                  </div>
                  <div className='_profile_box' >
                    <div className='profile_label'>
                      <CustomLabelText forlabel="title" text="Title" style={{fontSize: 15, fontWeight: 'bold'}}/>
                      <br />
                      <CustomTextInput
                        type='text'
                        name='title'
                        placeholder='Enter your title'
                        onChange={formikProps.handleChange}
                        value={formikProps.values.title}
                        style={{width: '100%'}}
                      />
                      <ErrorMessage errorValue={formikProps.errors.title}/>
                    </div>
                    <div className='text_label'>
                      <CustomLabelText forlabel="bio" text="Bio" style={{fontSize: 15, fontWeight: 'bold'}}/>
                      <br />
                      <CustomTextArea 
                        name="bio"
                        placeholder='Enter your bio information'
                        onChange={formikProps.handleChange}
                        value={formikProps.values.bio}
                        
                      />
                      <ErrorMessage errorValue={formikProps.errors.bio}/>
                    </div>
                    <br/>
                    <CustomButton 
                      type="submit"
                      value="Next"  
                      style={{ width: 100,padding: 8,marginLeft: '68%' }}  
                    />
                    
                  </div>
                </form>
            )}
        </Formik>
    </Fragment>
  );
};

export default ProfileAbout;





