import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';

import './transfer.css'
import './card.css'

const BankTransfer = () => {
    return (
       <Fragment>
            <div class="overlay-grayed">
                <div class="overlay-txt">
                    <label class="overlay-bank">First Bank</label><br/>
                    <span class="overlay-acc">0123456789 <i class="fa fa-paste acc"></i></span><br/>
                    <span class="overlay-time">Expires in 23:14</span>
                </div>
                <div class="overlay_btn">
                    <button class="overlay_btnzs">Pay</button>
                </div>
            </div>
       </Fragment>
    );
};

export default BankTransfer;