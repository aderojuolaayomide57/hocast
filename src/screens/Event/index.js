import React, { Component } from 'react';
import Header from '../Dashboard/Header';
import { Link } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';
import ErrorMessage from '../../components/ErrorMessage/errorMessage';
import CustomButton from '../../components/Button';
import CustomTitleText from '../../components/Title';
import CustomLabelText from '../../components/Label';
import CustomTextInput from '../../components/TextInput';
import CustomTextArea from '../../components/TextArea';
import CustomSelect from '../../components/Select';
import GuestSpeakers from './GuestSpeakers';
import EventSession from './EventSession';
import Dropzone from '../../components/DragAndDrop';




import './event.css';

const validationSchema = Yup.object().shape({
   businessname: Yup.string()
   .label('Business Name')
   .required('Business Name is required'),
   location: Yup.string()
   .label('Business Location')
   .required('Business Name is required'),
})



class CreateEvent extends Component{

   constructor(props) {
       super(props);
       this.addOrCreateSession = this.addOrCreateSession.bind(this);
       this.addOrCreateSpeaker = this.addOrCreateSpeaker.bind(this);
       this.state = {
         dynamicSpeaker: [1],
         dynamicSession: [1]
       }
   }

   

    initialValues = {
      title: '',
      bio: '',
      businessname: "",
      location: ""
    }
    
    
     addOrCreateSession = (status) => {   
         let tempdynamicSession = this.state.dynamicSession;
         if(status === "add"){
            let lastdynamicSession = tempdynamicSession.length + 1 ;
            tempdynamicSession = [...tempdynamicSession, lastdynamicSession];
         }else {
            let fempdynamicSession = tempdynamicSession.pop();
         }
        
         this.setState({dynamicSession: tempdynamicSession });
         
   }
   addOrCreateSpeaker = (status) => {   
      let tempdynamicSpeaker = this.state.dynamicSpeaker;
      if(status === "add"){
         let lastdynamicSpeaker = tempdynamicSpeaker.length + 1 ;
         tempdynamicSpeaker = [...tempdynamicSpeaker, lastdynamicSpeaker];
      }else {
         let fempdynamicSpeaker = tempdynamicSpeaker.pop();
      }
      this.setState({dynamicSpeaker: tempdynamicSpeaker });
   }
     
   render() {
      return (
         <div className='eventpage'>
            <Header/> 
            <div className='container'>
               <div className='row'>
                  <div className='col-md-7 offset-md-2'>
                     <div className='event-title'>
                        <CustomTitleText
                           text="Create your event"
                           size="sm"
                           style={{paddingTop: 20,paddingBottom: 10, fontSize: 30, textAlign: 'center'}}
                        />
                        <p className='_eventP'>Create your event in less than 5 minutes</p>
                     </div>
                     <div className='event-form'>
                        <div className='input-box' >
                           <Formik
                              initialValues={this.initialValues}
                              onSubmit={values => this.props.onSubmit(values)}
                              validationSchema={validationSchema}
                           >
                              {formikProps => (
                                 <form onSubmit={formikProps.handleSubmit} >
                                    
                                       <Dropzone />
                                       <CustomLabelText 
                                          forlabel="eventname" 
                                          text="Event name" 
                                          style={{fontSize: 13, marginLeft: '5%', marginTop: 25, color: '#0000005e',fontWeight: '600'}}
                                       />
                                       <br />
                                       <CustomTextInput
                                          type='text'
                                          name='eventname'
                                          placeholder='Enter your event name'
                                          onChange={formikProps.handleChange}
                                          value={formikProps.values.eventname}
                                          style={{marginLeft: '5%',marginTop: 0, height: 40, padding: 10}}
                                       />
                                       <CustomLabelText 
                                          forlabel="category" 
                                          text="Select Categories" 
                                          style={{fontSize: 13, marginLeft: '5%', marginTop: 25, color: '#0000005e',}}
                                       />
                                       <br />
                                       <CustomSelect
                                          name="category"
                                          lists={['birthday','burial']}
                                          style={{marginLeft: '5%',marginTop: 0}}
                                       />
                                       <CustomLabelText 
                                          forlabel="businessname" 
                                          text="Description" 
                                          style={{fontSize: 13, marginLeft: '5%', marginTop: 25, color: '#0000005e'}}
                                       />
                                       <br />
                                       <CustomTextArea 
                                          name="bio"
                                          placeholder='Enter your bio information'
                                          onChange={formikProps.handleChange}
                                          value={formikProps.values.bio}
                                          cols={64}
                                          style={{marginLeft: '5%', marginTop: 0}}
                                       />
                                       <div className="row" style={{width: '94%', marginLeft: 'auto', marginRight: 'auto'}}>
                                          <div className="col-md-8">
                                             <CustomTitleText
                                                text="Guest Speakers"
                                                size="sm"
                                                style={{paddingTop: 5,paddingBottom: 1, fontSize: 16, marginBottom: 0}}
                                             />
                                             <p className='subHeader'>Your maximum guest speaker(s) is 1. 
                                             <Link to="#">View subscription plan to upgrade</Link></p>
                                          </div>
                                          <div className="col-md-3" style={{width: '98%', marginLeft: 44, paddingRight: 0,}}>
                                                <div className='gust-form' style={{width: 100, float: 'right',marginTop: 20}}>
                                                   <button type='button' onClick={() => this.addOrCreateSpeaker('remove')}>-</button>
                                                   <span>{this.state.dynamicSpeaker.length}</span>
                                                   <button type='button' onClick={() => this.addOrCreateSpeaker('add')}>+</button>
                                                </div>
                                          </div>
                                       </div>
                                       {
                                          this.state.dynamicSpeaker.length > 0 &&
                                          this.state.dynamicSpeaker.map((item, index) => {
                                                return (
                                                <GuestSpeakers
                                                   key={item} 
                                                   item={item}
                                                   formikProps={formikProps}
                                                   style={{width: '98%', marginLeft: 'auto', marginRight: 'auto'}}
                                                />
                                                );
                                             })
                                       }
                                    
                                       <div className="row" style={{width: '94%', marginLeft: 'auto', marginRight: 'auto'}}>
                                          <div className="col-md-8">
                                             <CustomTitleText
                                                text="Event Sessions"
                                                size="sm"
                                                style={{paddingTop: 5,paddingBottom: 1, fontSize: 16, marginBottom: 0}}
                                             />
                                             <p className='subHeader'>Your maximum session(s) is 1. 
                                             <Link to="" >View subscription plan to upgrade</Link></p>
                                          </div>
                                          <div className="col-md-3" style={{width: '98%', marginLeft: 44, paddingRight: 0,}}>
                                                <div className='gust-form' style={{width: 100, float: 'right',marginTop: 20}}>
                                                   <button type='button' onClick={() => this.addOrCreateSession('remove')}>-</button>
                                                   <span>{this.state.dynamicSession.length}</span>
                                                   <button type='button' onClick={() => this.addOrCreateSession('add')}>+</button>
                                                </div>
                                          </div>
                                       </div>
                                       {
                                          this.state.dynamicSession.length > 0 &&
                                          this.state.dynamicSession.map((item, index) => {
                                                return (
                                                <EventSession
                                                   key={item} 
                                                   item={item}
                                                   formikProps={formikProps}
                                                   style={{width: '95%', marginLeft: 'auto', marginRight: 'auto'}}
                                                />
                                                );
                                             })
                                       }

                                       <CustomButton 
                                             type="submit"
                                             value="Publish"  
                                             style={{ width: '93%', marginLeft: '5%',marginTop: 20 }}  
                                       />
                                 </form>
                                 )}
                           </Formik> 
                        </div>
                        
                     </div>
                  </div>
               </div>
            </div>
            <br></br>
            <br></br>
            <br></br>
         </div>
      );
   }   
};

export default CreateEvent;