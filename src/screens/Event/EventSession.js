import React from 'react';
import Header from '../Dashboard/Header';
import { Link } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';
import ErrorMessage from '../../components/ErrorMessage/errorMessage';
import CustomLabelText from '../../components/Label';
import CustomTextInput from '../../components/TextInput';
import CustomSelect from '../../components/Select';



import './event.css';


const EventSession=(props) => {
    const inputname = `sessionname ${props.item}`
    const price = `ticketprice ${props.item}`
    const startdate = `startdate ${props.item}`
    const starttime = `starttime ${props.item}`
    const enddate = `enddate ${props.item}`
    const endtime = `endtime ${props.item}`

    return (
        <div className='' style={props.style}>
          
            <div className="row">
                <div className='col-md-6'>
                    <CustomLabelText 
                        forlabel={inputname} 
                        text={`Session ${props.item}`}
                        style={{fontSize: 13, marginLeft: '5%', marginTop: 5, color: '#0000005e'}}
                    />
                    <br />
                    <CustomTextInput
                        type='text'
                        name={inputname}
                        placeholder='Enter your session name'
                        onChange={props.formikProps.handleChange}
                        value={props.formikProps.values.inputname}
                        style={{marginLeft: '5%',marginTop: 0, width: '95%', height: 40, padding: 10}}
                    />
                </div>
                <div className='col-md-6'>
                    <CustomLabelText 
                        forlabel={price} 
                        text={`Ticket Price ${props.item}`} 
                        style={{fontSize: 13, marginLeft: '5%', marginTop: 5, color: '#0000005e'}}
                    />
                    <br />
                    <CustomSelect
                        name={price}
                        lists={['free','burial']}
                        style={{marginLeft: '5%',marginTop: 0, width: '95%'}}
                    />
                </div>
            </div>
            <div className="row">
                <div className='col-md-6'>
                    <div className="row">
                        <div className='col-md-6'>
                            <CustomLabelText 
                                forlabel={startdate}
                                text="Start Date" 
                                style={{fontSize: 13, marginTop: 5, color: '#0000005e'}}
                            />
                            <br />
                            <CustomTextInput
                                type='date'
                                name={startdate}
                                onChange={props.formikProps.handleChange}
                                value={props.formikProps.values.startdate}
                                style={{marginTop: 0, 
                                    height: 40,  
                                    width: 143,paddingLeft: 13,
                                    paddingRight: 0}}
                                min="2018-01-01" 
                                max="2050-12-31"
                            />
                            
                        </div>
                        <div className='col-md-6'>
                            <CustomLabelText 
                                forlabel={starttime} 
                                text="Start Time" 
                                style={{fontSize: 13,marginTop: 5, color: '#0000005e'}}
                            />
                            <br />
                            <CustomTextInput
                                type='time'
                                name={starttime}
                                onChange={props.formikProps.handleChange}
                                value={props.formikProps.values.starttime}
                                style={{marginTop: 0,  height: 40, padding: 10}}
                                min="09:00" 
                                max="18:00"
                            />
                        </div>
                    </div>
                </div>
                <div className="col-md-6">
                    <div className="row">
                            <div className='col-md-6'>
                                <CustomLabelText 
                                    forlabel={enddate} 
                                    text="End Date" 
                                    style={{fontSize: 13, marginTop: 5, color: '#0000005e'}}
                                />
                                <br />
                                <CustomTextInput
                                    type='date'
                                    name={enddate}
                                    onChange={props.formikProps.handleChange}
                                    value={props.formikProps.values.enddate}
                                    style={{marginTop: 0, 
                                        height: 40, 
                                        width: 143,paddingLeft: 13,
                                        paddingRight: 0}}
                                    min="2018-01-01" 
                                    max="2050-12-31"
                                />
                            </div>
                            <div className='col-md-6'>
                                <CustomLabelText 
                                    forlabel={endtime} 
                                    text="End Time" 
                                    style={{fontSize: 13,  marginTop: 5, color: '#0000005e'}}
                                />
                                <br />
                                <CustomTextInput
                                    type='time'
                                    name={endtime}
                                    onChange={props.formikProps.handleChange}
                                    value={props.formikProps.values.endtime}
                                    style={{marginTop: 0, height: 40, padding: 10}}
                                    min="09:00" 
                                    max="18:00"
                                />
                            </div>
                    </div>
                </div>
            </div>
        
        </div>
    );
};

export default EventSession;