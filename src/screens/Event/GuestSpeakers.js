import React from 'react';
import Header from '../Dashboard/Header';
import { Link } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';
import ErrorMessage from '../../components/ErrorMessage/errorMessage';
import CustomButton from '../../components/Button';
import CustomTitleText from '../../components/Title';
import CustomLabelText from '../../components/Label';
import CustomTextInput from '../../components/TextInput';


import './event.css';


const GuestSpeakers= (props) => {
    const inputname = `guest ${props.item}`
    const email = `email ${props.item}`
    return (
        <div className='row' style={props.style}>
          
          <div className='col-md-6'>
            <CustomLabelText 
                forlabel={inputname} 
                text={`Guest ${props.item}`} 
                style={{fontSize: 13, marginLeft: '5%', marginTop: 5, color: '#0000005e'}}
            />
            <br />
            <CustomTextInput
                type='text'
                name={inputname}
                placeholder='Enter guest name'
                onChange={props.formikProps.handleChange}
                value={props.formikProps.values.inputname}
                style={{marginLeft: '5%',marginTop: 0, width: '95%', height: 40, padding: 10}}
            />
          </div>
          <div className='col-md-6'>
            <CustomLabelText 
                forlabel={email}
                text="Email" 
                style={{fontSize: 13, marginLeft: '5%', marginTop: 5, color: '#0000005e'}}
            />
            <br />
            <CustomTextInput
                type='text'
                name={email}
                placeholder='Enter guest email address'
                onChange={props.formikProps.handleChange}
                value={props.formikProps.values.email}
                style={{marginLeft: '5%',marginTop: 0, width: '95%', height: 40, padding: 10}}
            />
          </div>
        
        </div>
    );
};

export default GuestSpeakers;