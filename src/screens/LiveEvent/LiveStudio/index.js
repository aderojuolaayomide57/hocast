import React, { Component } from 'react';
import Header from '../../Dashboard/Header';
import { Link } from 'react-router-dom';
import CustomTitleText from '../../../components/Title';
import CustomLink from '../../../components/Link';
import CustomButton from '../../../components/Button';
import LiveEngageBox from '../components/Chats/LiveEngageBox';
import MeetingBox from '../../LiveEvent/components/MeetingBox';
import '../liveevent.css';
import './livestudio.css';
class LiveStudio extends Component{

  constructor(props){
    super(props)
      this.state = {
        isActivated: false
      }
  }

  showSideBox = () => {
    this.setState({isActivated: !this.state.isActivated})
  }

  render(){
    return (
        <div className='livestudiopage'>
           <Header/> 
           <div className="row" style={{height: '100%'}}>
            <div className={this.state.isActivated ? 'col-md-9' : 'col-md-12'}>
                <div className='livestudio-header'>
                    <div className='holder'>
                       <CustomTitleText
                              text="Tech & Post COVID-19"
                              size="sm"
                              style={{color: 'white', paddingBottom: 0, marginBottom: 0, fontSize: 16}}
                          />
                       <span className='by-os'>By Josh Osazuwa</span>
                    </div>  
                    <div className='icon-box'>
                        <label className='timer'>00:20</label>
                        <label className='status'>Live</label>
                        <CustomButton
                          type="button"
                          value="End"
                          btnStyleType="secondaryBtn"
                          //onClick={() => this.showSideBox()}
                          style={{padding: 2,fontSize: 13,paddingLeft: 7,paddingRight: 7,borderRadius: 3}}
                        />
                        
                        <span className='icon-event'><i className="fa fa-expand-wide"></i></span>
                        <span className='icon-event'><i className="fa fa-volume-up"></i></span>
                        <span className='icon-event'><i className="fa fa-cog"></i></span>
                        <span className='icon-event'
                        onClick={() => this.showSideBox()}
                        ><i className="fa fa-arrow-right"></i>
                        </span>
                    </div>  
                </div> 
                <div className="row" style={this.state.isActivated ? {paddingLeft: 25,paddingRight: 25, paddingTop: 10} : {paddingTop: 10}}>
                  <MeetingBox/>
                </div>    
            </div>
            { this.state.isActivated && 
                <LiveEngageBox/>
            }
          </div>  
        </div>
    );
  }  
};

export default LiveStudio;