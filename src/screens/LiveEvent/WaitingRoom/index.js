import React, { Component } from 'react';
import Header from '../../Dashboard/Header';
import { Link } from 'react-router-dom';
import CustomTitleText from '../../../components/Title';
import CustomLink from '../../../components/Link';
import CustomButton from '../../../components/Button';
import EngageBox from '../components/Chats/EngageBox';


import '../liveevent.css'
class WaitingRoom extends Component{

  constructor(props){
    super(props)
      this.state = {
        isActivated: false
      }
  }

  showSideBox = () => {
    this.setState({isActivated: !this.state.isActivated})
  }

  render(){
    return (
        <div className='liveeventpage'>
           <Header/> 
           <div className="row" style={{height: '100%'}}>
            <div className={this.state.isActivated ? 'col-md-9' : 'col-md-12'}>
                <div className='_liveevent'>
                      <button className='_empty'></button>
                      <div className='_live-title'>
                          <CustomTitleText
                              text="Tech & Post COVID-19"
                              size="sm"
                              style={{color: 'white', paddingBottom: 0, marginBottom: 0}}
                          />
                          <p className='_live-date'>23 April 2020 | 3:00PM</p>
                      </div>
                      <CustomLink 
                        text="Going live in"
                        page="/livestudio"
                        style={{  
                            paddingTop: '1.6%',
                            paddingLeft: '2%',
                            paddingRight: '2%',
                            fontSize: 'larger',
                            border: 'none'
                        }}  
                  />
                </div>
                <div className='_live-countdown'>
                    <div className='_live01' >
                      <h1 className='xy01' style={{borderLeft: 'none'}}>00</h1>
                      <p className='xy_01'>Days</p>
                    </div>
                    <div className='_live02'>
                      <h1 className='xy02'>05</h1>
                      <p className='xy_02'>Hours</p>
                    </div>
                    <div className='_live03'>
                      <h1 className='xy03'>50</h1>
                      <p className='xy_03'>Minutes</p>
                    </div>
                    <div className='_live04'>
                      <h1 className='xy04'>28</h1>
                      <p className='xy_04'>Seconds</p>
                    </div>
                </div>
                <div className='live_link'>
                    <Link to="#" className='avs'>Audio & Video Settings</Link>
                    <Link to="/livestudio" className='gli'>Go Live </Link>
                    <CustomButton
                          type="button"
                          value="Chat"
                          btnStyleType="secondaryBtn"
                          onClick={() => this.showSideBox()}
                          icon={<span className='top-num'>3</span>}
                          style={{paddingLeft: 20, paddingRight: 0}}
                    />
                </div>
              <div className='live-event-des'>
                  <p className='dmw'>Say hello to your attendees</p> 
              </div>
            </div>
            { this.state.isActivated && 
            <EngageBox
              showSideBox={() => this.showSideBox()}
            />}
          </div>  
        </div>
    );
  }  
};

export default WaitingRoom;