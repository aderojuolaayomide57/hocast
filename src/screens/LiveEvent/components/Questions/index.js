import React,{ Component } from 'react';
import ChatForm from '../chatForm';
import QuestionBox from './QuestionBox';



const QuestionRoom = (props) => {

    return (
        <div className='questionroom'>
            
            <QuestionBox />
            <ChatForm/>
        </div>
    )
     
}

export default QuestionRoom;