import React, { Fragment } from 'react';

const QuestionBox = (props) => {

        return (
            <Fragment >
                <div className='questionbox'>
                        <div className='partiton'>
                            <img
                                src={process.env.PUBLIC_URL + '../../assets/image/1.png'}
                                alt='logo'
                                className='question-visitor'
                            />
                            <div className='content-question'>
                                <h2 className='vistor-name-question'>Josh</h2>
                                <span className='question-time'>12:00PM</span>
                                <p className='question-text'>Hello guys, glad you could join me. Really</p>
                            </div>
                            <i className="fa fa-check-circle" style={{color: 'green', marginLeft: 10,marginTop: 10}}></i>
                        </div>
                    </div>
            </Fragment>
        )
}

export default QuestionBox;