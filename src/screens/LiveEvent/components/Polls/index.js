import React from 'react';
import ChatForm from '../chatForm';
import PollBox from './pollBox';


const Polls = (props) => {

    return (
        <div className='chats'>
            
            <PollBox
                type='host'
            />
            <PollBox
                type='visitor'
            />
            <ChatForm/>
        </div>
    )
     
}

export default Polls;