import React, { Fragment } from 'react';

const PollBox = (props) => {

        return (
            <Fragment >
                <div className='_chts'>
                        <div className='partiton'>
                            <img
                                src={process.env.PUBLIC_URL + '../../assets/image/1.png'}
                                alt='logo'
                                className='chat-visitor'
                            />
                            <div className='content-visitor'>
                                <h2 className='visitor-name'>Josh (others)</h2>
                                <span className='visitor-time '>12:00PM</span>
                                <p className='chat-text'>Hello guys, glad you could join me. Really 
                                excited about the event. See you guys in a bit.</p>
                            </div>
                        </div>
                    </div>
            </Fragment>
        )
}

export default PollBox;