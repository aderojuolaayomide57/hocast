import React, { Fragment } from 'react';

const People = (props) => {

        return (
            <Fragment >
                <div className='peoplebox'>
                        <div className='people-partiton'>
                            <img
                                src={process.env.PUBLIC_URL + '../../assets/image/1.png'}
                                alt='logo'
                                className='people-img'
                            />
                            <div className='content-people'>
                                <h2 className='people-name'>Josh</h2>
                                <p className="people-text">oladola57@gmail.com</p>
                            </div>
                            <i className="icon-people-talking fa fa-align-center"></i>
                            <i className="icon-people-mic fa fa-microphone"></i>
                            
                        </div>
                    </div>
            </Fragment>
        )
}

export default People;