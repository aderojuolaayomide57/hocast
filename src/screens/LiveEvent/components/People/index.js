import React,{ Component } from 'react';
import People from './People';



const PeopleRoom = (props) => {

    return (
        <div className='peopleroom'>
            <div className="main-people-box">
                <h2 className='people-type'>Host (s)</h2>
                <span className='people-counter'>12</span>
                <People />
            </div>
            <div className="main-people-box">
                <h2 className='people-type'>Guest (s)</h2>
                <span className='people-counter'>12</span>
                <People />
                <People />
            </div>
            <div className="main-people-box">
                <h2 className='people-type'>Attendees (s)</h2>
                <span className='people-counter'>12</span>
                <People />
                <People />
                <People />
            </div>
        </div>
    )
     
}

export default PeopleRoom;