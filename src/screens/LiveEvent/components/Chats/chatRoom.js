import React,{ Component } from 'react';
import ChatForm from '../chatForm';
import ChatBox from './chatBox';



const ChatRoom = (props) => {

    return (
        <div className='chats'>
            
            <ChatBox
                type='host'
            />
            <ChatBox
                type='visitor'
            />
            <ChatBox
                type='visitor'
            />
            <ChatBox
                type='host'
            />
            <ChatForm/>
        </div>
    )
     
}

export default ChatRoom;