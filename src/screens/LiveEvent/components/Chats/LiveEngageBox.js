import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../../waitingroom.css';
import CustomButton from '../../../../components/Button';
import ChatRoom from './chatRoom';
import QuestionRoom from '../Questions';
import PeopleRoom from '../People';


const active = {
    borderBottom: "5px solid #F55B31",
    paddingBottom: 5,
}

class LiveEngageBox extends Component {

    constructor(props){
        super(props)
        this.state = {
            page: 'chat'
        }
    }

    switchTabs = (e,valPage) => {
        e.preventDefault();
        this.setState({page: valPage})
    }

    render(){
        return (
            <div className='col-md-3' style={{height: '100%'}}>
                    <div className='_waiting-box'>
                        <div className='liveengage-header'>
                                <ul className='tab-link'>
                                    <li className='tab-link-child' style={this.state.page === 'chat' ? active : {}}>
                                        <Link onClick={(e) => this.switchTabs(e,'chat')} to='#' >
                                            Chats</Link>
                                    </li>
                                    <li className='tab-link-child' style={this.state.page === 'question' ? active : {}}>
                                        <Link to='#' onClick={(e) => this.switchTabs(e,'question')}>Questions</Link></li>
                                    <li className='tab-link-child' style={this.state.page === 'poll' ? active : {}}>
                                        <Link to="#" onClick={(e) => this.switchTabs(e,'poll')}>Polls</Link></li>
                                    <li className='tab-link-child' style={this.state.page === 'people' ? active : {}}>
                                        <Link to="#" onClick={(e) => this.switchTabs(e,'people')}>People
                                    <span className='live-not-e'>30</span>
                                    </Link></li>
                                </ul>
                            <div className='live-header'>
                            {this.state.page === 'chat' && <label className='_livechat'>Live Chat</label>}
                            {this.state.page === 'question' && <label className='_livechat'>Live Question</label>}
                            {this.state.page === 'poll' && <label className='_livechat'>Live Poll</label>}
                            {this.state.page === 'people' && <label className='_livechat'>People</label>}
                            </div>
                        </div>
                        {this.state.page === 'chat' && <ChatRoom /> }
                        {this.state.page === 'question' && <QuestionRoom/>}
                        {this.state.page === 'people' && <PeopleRoom/>}
                    </div>
            </div>
        );
    }
   
};

export default LiveEngageBox;