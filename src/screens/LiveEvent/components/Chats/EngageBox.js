import React, { Component } from 'react';

import CustomButton from '../../../../components/Button';
import ChatRoom from './chatRoom';
import '../../waitingroom.css';


class EngageBox extends Component {

        constructor(props){
            super(props)
            this.state = {
                page: ''
            }
        }

        switchTabs = (valPage) => {
            this.setState({page: valPage})
        }
        

        render(){
          return (
             <div className='col-md-3' style={{height: '100%'}}>
                   <div className='_waiting-box'>
                        <div className='waiting-header'>
                                <CustomButton
                                        type="button"
                                        value="Waiting Room Chat"
                                        btnStyleType="secondaryBtn"
                                        onClick={() => this.switchTabs('chat')}
                                        style={{paddingLeft: 15, paddingRight: 15,
                                                fontSize: 11, backgroundColor: 'transparent', 
                                                border: '1px solid lightgrey', marginLeft: 5, marginRight: 15}}
                                />
                                <CustomButton
                                        type="button"
                                        value="Poll"
                                        btnStyleType="secondaryBtn"
                                        onClick={() => this.props.showSideBox('poll')}
                                        style={{paddingLeft: 15, paddingRight: 15,
                                                fontSize: 11, backgroundColor: 'transparent', 
                                                border: '1px solid lightgrey',  marginRight: 15}}
                                />
                                <CustomButton
                                        type="button"
                                        value="x"
                                        btnStyleType="secondaryBtn"
                                        onClick={() => this.props.showSideBox()}
                                        style={{fontSize: 17, backgroundColor: 'transparent',marginLeft: '9%'}}
                                />
                        </div>
                        <ChatRoom />
                   </div>
              </div>
          );
        }
   
};

export default EngageBox;