import React from 'react';
import CustomButton from '../../../components/Button';


const chatForm = () => {
    return (
        <div className='cht-input'>
            <input type='text' placeholder='Say something ....' className='typing'/>
            <CustomButton
                type="submit"
                value="Send"
                btnStyleType="secondaryBtn"
                icon={<i className="fa fa-location-arrow"></i>}
                style={{paddingLeft: 15, paddingRight: 15,
                        fontSize: 11, backgroundColor: 'transparent', 
                        marginRight: 1,
                    float: 'right'}}
            />
        </div>
    );
};

export default chatForm;