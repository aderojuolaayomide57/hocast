import React, {useState, useEffect} from 'react';
import { Link, useLocation, useHistory } from 'react-router-dom';
import ProgressComponent from '@material-ui/core/CircularProgress';



const MeetingBox = (props) => {

    const [loading, setLoading] = useState(true);

    const containerStyle = {
        width: '100%',
        height: '400px',
    };

    const jitsiContainerStyle = {
        display: (loading ? 'none' : 'block'),
        width: '100%',
        height: '100%',
    }

    
    const startConference = () => {
        try {
         const domain = 'meet.jit.si';
            //const domain = '8x8.vc';
            const options = {
                roomName: 'JitsiMeetAPIExample',
                width: '100%',
                height: 540,
                configOverwrite: { startWithAudioMuted: true },
                interfaceConfigOverwrite: { DISABLE_DOMINANT_SPEAKER_INDICATOR: true },
                parentNode: document.getElementById('jitsi-container'),
                    interfaceConfigOverwrite: {
                    filmStripOnly: false,
                    SHOW_JITSI_WATERMARK: false,
                },
                
                configOverwrite: {
                    disableSimulcast: true,
                },
            };

         
      
         const api = new window.JitsiMeetExternalAPI(domain, options);
         setLoading(false);
         api.addEventListener('videoConferenceJoined', () => {
          console.log('Local User Joined');
          setLoading(false);
          api.executeCommand('displayName', 'MyName');
         });
        } catch (error) {
         console.error('Failed to load Jitsi API', error);
        }
       }

       useEffect(() => {
        // verify the JitsiMeetExternalAPI constructor is added to the global..
        //window.JitsiMeetExternalAPI = window.JitsiMeetExternalAPI || window.exports.JitsiMeetExternalAPI;
        if (window.JitsiMeetExternalAPI) startConference();
        else alert('Jitsi Meet API script not loaded');
       }, []);

    return (
        <div className='col-md-12'>
            <div style={containerStyle}>
            {loading && <ProgressComponent />}
            <div
                id="jitsi-container"
                style={jitsiContainerStyle}
            />
            </div>
        </div>
    )
     
}

export default MeetingBox;