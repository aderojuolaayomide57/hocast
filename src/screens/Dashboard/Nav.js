import React, {useState, useEffect} from 'react';
import { Link, useLocation, useHistory } from 'react-router-dom';
import CustomButton from '../../components/Button';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import './userprofile.css';
import { Creators } from '../../services/redux/auth/actions';
import { persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage';



const active = {
    color: "#F55B31",
    borderBottom: "5px solid #F55B31",
    paddingBottom: 25,
}



const Nav= (props) => {
    let location = useLocation();
    let history = useHistory();
    const Logout = (e) => {
        e.preventDefault();
        storage.removeItem('token')
        storage.removeItem('persist:auth')
        storage.removeItem('persist:user')
        storage.removeItem('persist:root')
        history.replace('/'); 
    }

    const [state, setState] = useState({dropdown: false});
    const showSideMenu = () => {}

    let { dropdown } = state;

    return (
        <div>
            <div className='nav'>
                 <div className='_box-containerlogo_container'>
                    <img src={process.env.PUBLIC_URL + '../../assets/image/Group 2896.svg'} 
                    className='dashboard_logo'
                    alt='Logo'
                    />
                 </div>
                 <div className='dash_links' >
                        <ul className='list-link'>
                            <li className='list-link-child'><Link to="/dashboard" style={location.pathname === '/dashboard' ? active: {}}>Dashboard</Link></li>
                            <li className='list-link-child'><Link to='/createevent' style={location.pathname === '/createevent' ? active: {}}>Create Event</Link></li>
                            <li className='list-link-child'><Link to="/waitingroom" style={location.pathname === '/waitingroom' ? active: {}}>Live Studio</Link></li>
                            <li className='list-link-child'><Link to="#">Analytics</Link></li>
                        </ul>
                 </div>
                 <div className='topImageCase'>
                    <img
                        src={process.env.PUBLIC_URL + '../../assets/image/1.png'}
                        alt='logo'
                        className='image_icon'
                    />
                    <i onClick={() => setState({dropdown: !dropdown})} className="fa fa-angle-down"></i>
                    { dropdown && 
                        <div className="dropDownBox">
                            <ul>
                                <li><Link onClick={(e) => Logout(e)} style={{borderTop: 'none'}} to="#">Logout</Link></li>
                                <li><Link to="/profile" >Profile</Link></li>
                            </ul>
                        </div>
                    }
                 </div>
                 <CustomButton
                    type="button"
                    btnStyleType="secondaryBtn"
                    icon={<i className="fa fa-bars" ></i>}
                    style={{height: 35, paddingTop: 5}}
                    onClick={() => showSideMenu()}
                />
            </div>
        </div>
    );
};


Nav.protoTypes = {
    
};
  
const mapDispatchToProps = dispatch => {
    return {
        loginOut: () => {
            dispatch(Creators.login())
        },
    }
}
    
export default connect(null, mapDispatchToProps)(Nav);

