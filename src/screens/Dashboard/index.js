 import React, { Component, Fragment } from 'react';
 import './userprofile.css'
 import { withRouter } from "react-router";
 import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { Creators } from '../../services/redux/user/actions';
import Header from'./Header';
import SideBar from'./SideBar';
import MiddleRow from'./MiddleRow';
import LeftRow from'./LeftRow';
import ProgressComponent from '@material-ui/core/CircularProgress';
import store from '../../services/redux/store';



class UserProfiles extends Component{
    
  constructor(props){
    super(props);
    this.state = {
      loading: false
    }
    const { match, location, history } = this.props;
    window.localStorage.removeItem('profile_page');
    if (!this.props.isAuthenticated) {
      history.replace('/'); 
    }
  }

  componentDidMount() {
    setTimeout(function() { //Start the timer
        this.setState({loading: true}) //After 1 second, set render to true
    }.bind(this), 1100)
  }

  

  componentWillMount() {
    if(store.getState().auth.token){
      this.props.getUserProfile();
    }else{
      this.props.history.push(`/`)
    }
  }

  render(){
    
    return (
      <div className='UseProfile'>
          <Header/>

          {!this.state.loading && <ProgressComponent style={progressStyle}/>}
          <div className='content' style={{ overflowY: 'scroll', height: 'calc(100vh - 50px)',width: '96%' }}>
          {this.state.loading &&  
            <Fragment>
              <SideBar 
                {...this.props}
              />
              <MiddleRow/>
              <LeftRow/>
            </Fragment>
          } 
          </div>
         
      </div>
    );
  }
};


UserProfiles.protoTypes = {
  user: PropTypes.object.isRequired,
};

const progressStyle = {color: '#CA6144', marginLeft: "45%", marginTop: "20%"}


const mapStateToProps = (state) => ({
  user: state.user.user,
  isAuthenticated: state.auth.isAuthenticated,
});

const mapDispatchToProps = dispatch => {
  return {
    getUserProfile: () => {
      dispatch(Creators.getUserDetails())
    }
  }
}

const UserProfile = withRouter(UserProfiles)

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);
