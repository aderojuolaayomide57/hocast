
import React, { Fragment, useState } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { Link, useHistory, useLocation } from 'react-router-dom';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import ErrorMessage from '../../components/ErrorMessage/errorMessage';
import CustomTextInput from '../../components/TextInput';
import CustomButton from '../../components/Button';
import { Creators } from '../../services/redux/auth/actions';
import CustomLabelText from '../../components/Label';
import PublicHeader from '../../components/PublicHeader';
import CustomTitleText from '../../components/Title';

import './usersign.css';


const UserSignup = (props) => {
    let history = useHistory();
    let location = useLocation();
    //let { from } = location.state || { from: { pathname: '/profile' } };
    const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

    const formik = useFormik({
        initialValues: {
            fullname: '',
            email: '',
            phonenumber: '',
            password: ''
        },
        validationSchema: Yup.object({
            fullname: Yup.string()
                .label('fullname')
                .required('Fullname is required'),
            email: Yup.string()
                .label('Email')
                .email('Enter a valid email')
                .required('Email is required'),
            phonenumber: Yup.string().matches(phoneRegExp, 'Phone number is not valid'),
            password: Yup.string()
                .label('Password')
                .min(3, 'Password must be atleast 3 characters long')
                .required('Password is required'),
          }),
        onSubmit: values => {
          //props.registerUser(values)
        },
      });
    
    

    return (
       <div className='event_pageview'>
          <PublicHeader/>
          <div className='user-wrapper'>
               <div className='subuser'>
                    <CustomTitleText
                        text="Join the evolution of virtual events"
                        size="md"
                        style={{color: '#CA6144'}}
                    />
                  <p className='_userp'>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed<br/> diam nonumy eirmod tempor invidunt ut labore et dolore</p>
                  <span className='_userspan'>Already have an account? <Link to='/login' className='_usrlnk'>Log in</Link></span>
               </div>
               <div className='_userform'>
                   <form onSubmit={formik.handleSubmit} style={{width: 340}}>
                    <div className='-hgh'>
                        <CustomLabelText forlabel= "fullname" text="Full Name"
                        style={{color: '#756F86',lineHeight: 2, marginLeft: 0}}/>
                        <br />
                        <CustomTextInput
                            type='text'
                            name='fullname'
                            placeholder='Full Name'
                            onChange={formik.handleChange}
                            value={formik.values.fullname}
                            style={{width: "100%", 
                                    marginLeft: 0,
                                    border: '1px solid #C9C9C9',
                                    borderRadius: 5
                                }}
                        />
                         <ErrorMessage style={{marginLeft: 0}} errorValue={formik.errors.fullname}/>
                    </div>
                    <div className='-hgh'>
                        <CustomLabelText forlabel= "email" text="Email"
                        style={{color: '#756F86',lineHeight: 2, marginLeft: 0}}/>
                        <br />
                        <CustomTextInput
                            type='text'
                            name='email'
                            placeholder='Email'
                            onChange={formik.handleChange}
                            value={formik.values.email}
                            style={{width: "100%", 
                                    marginLeft: 0,
                                    border: '1px solid #C9C9C9',
                                    borderRadius: 5
                                }}
                        />
                         <ErrorMessage style={{marginLeft: 0}} errorValue={formik.errors.email}/>
                    </div>
                    <div className='-hgh'>
                        <CustomLabelText forlabel= "phonenumber" text="Phone Number"
                        style={{color: '#756F86',lineHeight: 2, marginLeft: 0}}/>
                        <br />
                        <CustomTextInput
                            type='tel'
                            name='phonenumber'
                            placeholder='Enter your phone number'
                            onChange={formik.handleChange}
                            value={formik.values.phonenumber}
                            style={{width: "100%", 
                                    marginLeft: 0,
                                    border: '1px solid #C9C9C9',
                                    borderRadius: 5
                                }}
                        />
                         <ErrorMessage style={{marginLeft: 0}} errorValue={formik.errors.phonenumber}/>
                    </div>
                    <div className='-hgh'>
                        <CustomLabelText forlabel= "password" text="Password"
                        style={{color: '#756F86',lineHeight: 2, marginLeft: 0}}/>
                        <br />
                        <CustomTextInput
                            type='password'
                            name='password'
                            placeholder='Enter your password'
                            onChange={formik.handleChange}
                            value={formik.values.password}
                            style={{width: "100%", 
                                    marginLeft: 0,
                                    border: '1px solid #C9C9C9',
                                    borderRadius: 5,
                                    
                                }}
                        />
                         <ErrorMessage style={{marginLeft: 0}} errorValue={formik.errors.password}/>
                    </div>
                    

                    <div className='user-term'>
                    
                        <p className='_terms'>
                            <input type="checkbox" name="terms"/> I agree to the <u>Terms</u> & <u>Privacy Policy</u>
                        <br/>  I agree to share my contact details with the host.</p>
                        
                    </div>
                    
                    <CustomButton 
                        type="submit"
                        value="Join event"  
                        style={{ width: '100%',
                         marginLeft: 0, 
                         height: 40,
                         fontSize: 20, 
                         fontWeight: 'bold',borderRadius: 5,padding: 0}}  
                    />
                        <br/> <br/> 
                   </form>
               </div>
               
          </div>
        </div> 
    );
};

UserSignup.protoTypes = {
    //visitor: PropTypes.object.isRequired,
    //isAuthenticated: PropTypes.bool.isRequired,
    //error: PropTypes.string,
  };

const mapDispatchToProps = dispatch => {
    return {
      
    }
}
  
const mapStateToProps = (state) => ({
    //visitor: state.visitor.visitor,
    //isAuthenticated: state.auth.isAuthenticated,
    //error: state.visitor.error_message,
});
  
export default connect(mapStateToProps, mapDispatchToProps)(UserSignup);
