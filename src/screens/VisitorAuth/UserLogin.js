import React, { Fragment, useState } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { Link, useHistory, useLocation } from 'react-router-dom';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import ErrorMessage from '../../components/ErrorMessage/errorMessage';
import CustomTextInput from '../../components/TextInput';
import CustomButton from '../../components/Button';
import { Creators } from '../../services/redux/auth/actions';
import CustomLabelText from '../../components/Label';
import PublicHeader from '../../components/PublicHeader';
import CustomTitleText from '../../components/Title';

import './userlogin.css';

const UserLogin= (props) => {
    let history = useHistory();
    let location = useLocation();

    const formik = useFormik({
        initialValues: {
            email: '',
            password: ''
        },
        validationSchema: Yup.object({
            email: Yup.string()
                .label('Email')
                .email('Enter a valid email')
                .required('Email is required'),
            password: Yup.string()
                .label('Password')
                .min(3, 'Password must be atleast 3 characters long')
                .required('Password is required'),
          }),
        onSubmit: values => {
          //props.registerUser(values)
        },
      });
    return (
       <div className='event_pageview'>
          <PublicHeader/>
          <div className='userlogin'>
                <div className='subuser'>
                        <CustomTitleText
                            text="Welcome back!"
                            size="md"
                            style={{color: '#CA6144'}}
                        />
                        <p className='_userps'>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed<br/> diam nonumy eirmod tempor invidunt ut labore et dolore</p>
                        <span className='_userspans'>Are you new to Hocast? <Link to='/' className='_usrlnk'>Sign in</Link></span>
                </div>
                <div className='userlogin-form'>
                   <form onSubmit={formik.handleSubmit} style={{width: 340}}>
                       <div className='klh'>
                            <CustomLabelText forlabel= "email" text="Email"
                                style={{color: '#756F86',lineHeight: 2, marginLeft: 0}}
                            />
                            <br />
                            <CustomTextInput
                                type='text'
                                name='email'
                                placeholder='Email'
                                onChange={formik.handleChange}
                                value={formik.values.email}
                                style={{width: "100%", 
                                        marginLeft: 0,
                                        border: '1px solid #C9C9C9',
                                        borderRadius: 5
                                    }}
                            />
                            <ErrorMessage style={{marginLeft: 0}} errorValue={formik.errors.email}/>
                        </div>
                        <div className='_labell-child'>
                            <CustomLabelText forlabel= "password" text="Password"
                            style={{color: '#756F86',lineHeight: 2, marginLeft: 0}}/>
                            <br />
                            <CustomTextInput
                                type='password'
                                name='password'
                                placeholder='Enter your password'
                                onChange={formik.handleChange}
                                value={formik.values.password}
                                style={{width: "100%", 
                                        marginLeft: 0,
                                        border: '1px solid #C9C9C9',
                                        borderRadius: 5,
                                    }}
                            />
                            <ErrorMessage style={{marginLeft: 0}} errorValue={formik.errors.password}/>
                        </div>
                                        
                        <div className='login_link'>
                            <div className='logged-in_boxs'>
                                <input type='radio' name='' className='_login-radio' />
                                <Link to='#' className='logged-ins'>
                                    Keep me logged in
                                </Link>
                            </div>
                            <div className='fgot-boxs'>
                                <Link to='#' className='fgot'>
                                    Forgot Password?
                                </Link>
                            </div>
                        </div>
                        <CustomButton 
                            type="submit"
                            value="Join event"  
                            style={{ width: '100%',
                            marginLeft: 0, 
                            height: 40,
                            fontSize: 20, 
                            fontWeight: 'bold',borderRadius: 5,padding: 0}}  
                        /> 
                    </form>
                </div>
          </div>
        </div> 
    );
};

UserLogin.protoTypes = {
    //visitor: PropTypes.object.isRequired,
    //isAuthenticated: PropTypes.bool.isRequired,
    //error: PropTypes.string,
  };

const mapDispatchToProps = dispatch => {
    return {
      
    }
}
  
const mapStateToProps = (state) => ({
    //visitor: state.visitor.visitor,
    //isAuthenticated: state.auth.isAuthenticated,
    //error: state.auth.error_message,
});
  
export default connect(mapStateToProps, mapDispatchToProps)(UserLogin);

