import React from 'react';
import { Link } from 'react-router-dom';
import './eventpage.css';
import PublicHeader from '../../components/PublicHeader';
import CustomTitleText from '../../components/Title';
import CustomLink from '../../components/Link';
import CustomButton from '../../components/Button';


const EventPageView= () => {
    return (
       <div className='event_pageview'>
          <PublicHeader/>
          <div className='container' id="eventview-wrapper">
               <div className="row">
                    <div className='col-md-6'>
                        <div className='view-header'>
                            <CustomTitleText
                                text="Tech & Post COVID-19"
                                size="sm"
                                style={{marginBottom: 0,marginLeft: 0}}
                            />
                            <p className='view-desc'>23 April, 2020 - 24 April, 2020</p>
                        </div>
                        <div className='image-view'>
                            <img
                                src={process.env.PUBLIC_URL + '../../assets/image/woman.png'}
                                alt='logo'
                                className='event-img'
                            />
                            <div className='image-dec'>
                                <h4 className='img-header'>DESCRIPTION</h4>
                                <p className='image-descr-w'>
                                Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                                sed diam nonumy eirmod tempor invidunt ut labore et 
                                dolore magna aliquyam erat, sed diam voluptua. 
                                At vero eos et accusam et justo duo dolores et ea
                                rebum. Stet clita kasd gubergren, no sea takimata 
                                sanctus est Lorem ipsum dolor sit amet. 
                                Lorem ipsum dolor sit amet, consetetur sadipscing
                                </p>
                            </div>
                        </div>
                        <div className='event-link'>
                            <label className='link-link'>SHARE EVENT LINK</label>
                            <Link to='/' className='lnk'>hocast/joshmart/tech & post covid19</Link>
                        </div>
                    </div>
                    <div className='col-md-5 event-info-form' >
                        <br/>
                        <CustomLink 
                            text="Going live in"
                            page="#"
                            style={{backgroundColor: '#566683', 
                                    color: "white", 
                                    border: 'none',
                                    padding: "10px 40%",
                                    borderRadius: 4,
                                    fontWeight: 500,
                                    height: 40,
                                }}
                        />
                        
                        <div className='_event-countdown'>
                            <div className='e_live002'>
                                <h1 className='eventy02' style={{borderLeft: 'none', color: '#566683'}}>00</h1>
                                <p className='e-xy_02'>Days</p>
                            </div>
                            <div className='e_live03'>
                                <h1 className='eventy03'>05</h1>
                                <p className='e-xy_03'>Hours</p>
                            </div>
                            <div className='e_live04'>
                                <h1 className='eventy04'>50</h1>
                                <p className='e-xy_04'>Minutes</p>
                            </div>
                            <div className='e_live05'>
                                <h1 className='eventy05'>28</h1>
                                <p className='e-xy_05'>Seconds</p>
                            </div>
                        </div>
                        <div className='event-owner'>
                            <div className='owner_live001' style={{borderLeft: 'none', paddingLeft: 0}}>
                                <p className='event-tt'>Host</p>
                                <h1 className='e-xy01'>Josh Osazuwa</h1>
                                <p className='e_xy_01'>Brand Designer & Strategist</p>
                            </div>
                            <div className='own_live002'>
                                <p className='event-tt'>Guest 1</p>
                                <h1 className='e-xy02'>Josh Osazuwa</h1>
                                <p className='e_xy_01'>Identity Designer</p>
                            </div>
                            <div className='onwer_live003'>
                                <p className='event-tt'>Guest 2</p>
                                <h1 className='e-xy03'>Josh Osazuwa</h1>
                                <p className='e_xy_01'>Inventor</p>
                            </div>
                            
                        </div>
                        <div className='event-ticketing'>
                            <h2 className='tt-head'>Ticket(s)</h2>
                            <p className='tt-p'>Select event sessions you wish to attend</p>
                            <div className='session-wrapper'>
                                <div className='sess-1'>
                                    <input type="radio" id="first-session" name="first-session"/>
                                    <div className='sess-child'>
                                        <p className='ses-label'>
                                            Session 1 - Introduction
                                        </p>
                                        <span className='sess-pan'>21 Nov, 12:00PM–1:00PM</span>
                                    </div>
                                    <label className='sess-free'>Free</label>
                                </div>
                                <div className='sess-1'>
                                    <input type="radio" id="first-session" name="first-session"/>
                                    <div className='sess-child'>
                                        <p className='ses-label'>
                                        Session 2 - Design Strategy
                                        </p>
                                        <span className='sess-pan'>21 Nov, 2:00PM–3:00AM</span>
                                    </div>
                                    <label className='sess-free'>NGN 3000.00</label>
                                </div>
                                <div className='sess-1'>
                                    <input type="radio" id="first-session" name="first-session"/>
                                    <div className='sess-child'>
                                        <p className='ses-label'>
                                        Session 3 - Design Strategy
                                        </p>
                                        <span className='sess-pan'>21 Nov, 12:00PM–1:00PM</span>
                                    </div>
                                    <label className='sess-free'>NGN 3000.00</label>
                                </div>
                                <div className='btn-sess'>
                                    <CustomButton
                                        type="submit"
                                        value="Checkout"
                                        btnStyleType="primary"
                                        style={{width: '100%'}}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
               </div>
               <br/><br/>
               <hr />
               <div className="row">
                    <div className='col-md-5'>
                        <div className='host'>
                            <CustomTitleText
                                text="About the host"
                                size="sm"
                                style={{marginBottom: 0,marginLeft: 0}}
                            />
                            <div className='n-name'>
                                <label className='abt-name'>NAME</label>
                                <h1 className='author'>Josh Osazuwa</h1>
                            </div>
                            <div className='bio-details'>
                                <label className='bio-name'>BIO</label>
                                <p className='about-bio'>
                                Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                                sed diam nonumy eirmod tempor invidunt ut labore et dolore
                                magna aliquyam erat, sed diam voluptua. 
                                At vero eos et accusam et justo duo dolores et ea rebum. Stet
                                </p>
                            </div>
                            <div className='org'>
                                <label className='org-name'>ORGANISATION</label>
                                <p className='org-details'>Social Media Week</p>
                            </div>

                        </div>
                    </div>
                    <div className='col-md-5 view-future-event'>
                      <div className='upcoming-e'>
                            <CustomTitleText
                                text="Upcoming events by host"
                                size="sm"
                                style={{marginBottom: 0,marginLeft: 0}}
                            />
                            
                            <div className='upcoming-event_display'>
                                <div className='event-display-img'>
                                    <img
                                    src={process.env.PUBLIC_URL + '../../assets/image/woman-1.png'}
                                    alt='logo'
                                    className='img-display-e'
                                    />
                                </div>
                                <div className='abt-event'>
                                    <h2 className='upcoming-tt'>Future of Digital World</h2>
                                    <span className='event-author'>By Josh Osazuwa</span>
                                    <label className='event-date'>1 June 2020 | 7:00PM</label>
                                    <p className='event-dsc'>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, 
                                        sed diam nonumy eirmod tempor invidunt ut labore et dolore
                                        magna aliquyam
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
               </div>
               <br/><br/>
          </div>
       </div>
    );
};

export default EventPageView;