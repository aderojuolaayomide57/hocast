import React, { Fragment, useState } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { Link, useHistory, useLocation } from 'react-router-dom';
//import { signup } from '../../actions/auth';

import { useFormik } from 'formik';
import * as Yup from 'yup';
import ErrorMessage from '../../components/ErrorMessage/errorMessage';
import CustomTextInput from '../../components/TextInput';
import CustomButton from '../../components/Button';
import { Creators } from '../../services/redux/auth/actions';
import CustomTitleText from '../../components/Title';
import CustomLabelText from '../../components/Label';
import ProgressComponent from '@material-ui/core/CircularProgress';




import './style.css';

const Signup = (props) => {
  let history = useHistory();
  let location = useLocation();
  let { from } = location.state || { from: { pathname: '/profile' } };
  

  const formik = useFormik({
    initialValues: {
      firstname: '',
      lastname: '',
      email: '',
      password: ''
    },
    validationSchema: Yup.object({
      firstname: Yup.string()
        .label('Firstname')
        .required('Firstname is required'),
      lastname: Yup.string()
        .label('Lastname')
        .required('Lastname is required'),
      email: Yup.string()
        .label('Email')
        .email('Enter a valid email')
        .required('Email is required'),
      password: Yup.string()
        .label('Password')
        .min(3, 'Password must be atleast 3 characters long')
        .required('Password is required'),
      }),
    onSubmit: values => {
      props.registerUser(values)
    },
  });

  if (props.isAuthenticated) {
    from ? history.replace(from) : history.replace('/profile');
  }

  return (
    <Fragment>
      <section>
        <div className='logo_box'>
          <div className="row">
            <div className="col-md-6">
                <img
                  src={process.env.PUBLIC_URL + '../../assets/image/Group 4.svg'}
                  alt='logo'
                  className='logo'
                />
                <p className='_ending'>
                  Hall of endless
                  <br /> possibility
                </p>
            </div>
            <div className="col-md-6">
              <div className='_form'>
              {props.isRegistering && <ProgressComponent style={progressStyle}/>}
                <Link to='/' className='_bck'>
                  Back
                </Link>
                <CustomTitleText
                  text="Sign Up"
                  size="md"
                  style={{textAlign: 'center',marginTop: 30}}
                />
                <Link to='/' className='_acc'>
                  Already have an Account
                </Link>
                <form className='_form-child' onSubmit={formik.handleSubmit}>
                  <div className='label-container'>
                    {
                      props.error && 
                      <div className='_labell'>
                        <ErrorMessage errorValue={props.error[0].msg}/>
                      </div>
                    }
                    <div className='_label'>
                      <div className='_first-box'>
                        <CustomLabelText style={{marginLeft: 0}} forlabel= "firstname" text="First Name"/>
                        <br />
                        <CustomTextInput
                          type='text'
                          name='firstname'
                          placeholder='Enter First Name'
                          onChange={formik.handleChange}
                          value={formik.values.firstname}
                          style={{width: "100%",marginLeft: 0}}
                      />
                        <ErrorMessage errorValue={formik.errors.firstname}/>
                      </div>
                      <div className='_last-box'>
                        <CustomLabelText style={{marginLeft: 0}} forlabel= "lastname" text="Last Name"/>
                        <br />
                        <CustomTextInput
                          type='text'
                          name='lastname'
                          placeholder='Enter Last Name'
                          onChange={formik.handleChange}
                          value={formik.values.lastname}
                          style={{width: "100%",marginLeft: 0}}
                      />
                        <ErrorMessage errorValue={formik.errors.lastname}/>
                      </div>
                    </div>
                    <div className='_labell'>
                      <CustomLabelText forlabel="email" text="Email"/>
                      <CustomTextInput
                          type='text'
                          name='email'
                          placeholder='Enter Email Address'
                          onChange={formik.handleChange}
                          value={formik.values.email}
                      />
                        <ErrorMessage errorValue={formik.errors.email}/>
                    </div>
                    <div className='_labell'>
                      <CustomLabelText forlabel = "password" text="Password" />
                      <CustomTextInput
                          type='password'
                          name='password'
                          placeholder='Enter your password'
                          onChange={formik.handleChange}
                          value={formik.values.password}
                      />
                        <ErrorMessage errorValue={formik.errors.password}/>
                    </div>
                    <span className='icon'>
                      {' '}
                      <img
                        src={
                          process.env.PUBLIC_URL + '../../assets/image/eye-24px.svg'
                        }
                        alt='icon'
                      />
                    </span>
                    <CustomButton 
                        type="submit"
                        value="Sign Up"  
                        style={{ width: '90%', marginLeft: 14}}  
                    />
                    <br></br>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>
    </Fragment>
  );
};

const progressStyle = {color: '#CA6144', width: 20, height: 20, marginLeft: 7, marginTop: 7}

Signup.protoTypes = {
  user: PropTypes.object.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  error: PropTypes.string,
  isRegistering: PropTypes.bool.isRequired,
};

const mapDispatchToProps = dispatch => {
  return {
    registerUser: data => {
      dispatch(Creators.registerRequest(data))
    }
  }
}

const mapStateToProps = (state) => ({
  user: state.auth.user,
  isAuthenticated: state.auth.isAuthenticated,
  error: state.auth.error_message,
  isRegistering: state.auth.isRegistering,
});

export default connect(mapStateToProps,mapDispatchToProps)(Signup);
