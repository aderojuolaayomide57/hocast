import React from 'react';
import PropTypes from 'prop-types';
import './style.css';


const CustomSelect = props => {

    return (
        
        <select name={props.name} className='gen-select' style={props.style}>
            <option value="">Select Categories</option>
            {props.lists.map((index,data) => <option key={index} value={data}>{data}</option>)}
        </select>
    )
}

CustomSelect.propTypes = {
    name: PropTypes.string.isRequired,
    style: PropTypes.shape({}),
    lists: PropTypes.array.isRequired
}

CustomSelect.defaultProps = {
    name: null,
    style: null
}

export default CustomSelect;