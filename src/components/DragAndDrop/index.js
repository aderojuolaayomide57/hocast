import React, { Component} from 'react';
import Dropzone from 'react-dropzone';

const baseStyle = {
  flex: 1,
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  borderWidth: 1,
  borderRadius: 2,
  borderColor: '#00000026',
  borderStyle: 'dashed',
  backgroundColor: "#00000005",
  color: '#bdbdbd',
  outline: 'none',
  transition: 'border .24s ease-in-out',
  width: '95%',
  height: 200,
  marginLeft: 'auto',
  marginRight: 'auto'
};

const activeStyle = {
  borderColor: '#2196f3'
};

const acceptStyle = {
  borderColor: '#00e676'
};

const rejectStyle = {
  borderColor: '#ff1744'
};

class MyDropzone extends Component {

  constructor(props){
    super(props);
    this.onDrop = (files) => {
      //this.setState({files})
      this.setState({files,checker: true}, () => {this.state.files.map(file => Object.assign(file, {
        preview: URL.createObjectURL(file)
      }))});
    };
    this.state = {
      files: [],
      checker: false,
    }
  }

  render() {
  
    const newbaseStyle = {
      width: "100%",
      height: 200,
    };


    
    const file = this.state.files ? this.state.files[0] : ''
    return (
      <Dropzone accept='image/jpeg, image/png' multiple={false} onDrop={this.onDrop}>
        {({getRootProps, getInputProps}) => (
          <div className="container">
            <div {...getRootProps({style: baseStyle})}>
              <input {...getInputProps()} />
                {
                  !this.state.checker && 
                  <div>
                    <span className='upload-icon' ><i className="fa fa-upload"></i></span>
                    <div className='sub-upload'>
                      <p className='upload-txt'>Drag & Drop or click to add event image</p>
                      <div className='upload_sub-txt'>JPEG or PNG (2160x1920px)</div>
                    </div>
                  </div>
                }
                { this.state.checker && 
               
                <img
                  src={file.preview}
                  style={newbaseStyle}
                />
                
                }
            </div>
          </div>
        )}
      </Dropzone>
    );
  }
}

export default MyDropzone;