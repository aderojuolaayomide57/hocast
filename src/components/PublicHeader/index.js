import React from 'react';
import PropTypes from 'prop-types';
import './style.css';



const PublicHeader = props => {

    return (
        
        <div className='event-nav'>
              <div className='E-img-wrapper'>
                  <img src={process.env.PUBLIC_URL + '../../../assets/image/Group5.svg'}
                    alt='logo'
                    className='public-logo'
                  />
              </div>
              <div className='e-sign'>
                  <span className='e-icon'>
                  <i class="fa fa-bars"></i>
                  </span>
              </div>
          </div>
    )
}

PublicHeader.propTypes = {
    style: PropTypes.shape({}),
}

PublicHeader.defaultProps = {
    style: null
}

export default PublicHeader;