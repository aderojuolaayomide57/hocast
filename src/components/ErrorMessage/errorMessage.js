import React from 'react';
import PropTypes from 'prop-types';
import './style.css';
import { defaults } from 'lodash';


const ErrorMessage = (props) => (
    <div>
        <p className="error" style={props.style}> {props.errorValue} </p>
    </div>
)

ErrorMessage.propTypes = {
    errorValue: PropTypes.string,
    style: PropTypes.shape({}),
}

ErrorMessage.defaultProps = {
    errorText: null,
    style: null,
}

export default ErrorMessage;