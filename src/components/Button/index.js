import React from 'react';
import PropTypes from 'prop-types';
import './style.css';




const CustomButton = (props) => {
    
    let selectedButton;
    if (props.btnStyleType === "secondaryBtn") {
        selectedButton = "primary secondaryBtn";
    }else{
        selectedButton = props.btnStyleType;
    }
    

    return (
        <button
            type={props.type}
            className={selectedButton}
            style={props.style}
            onClick={props.onClick ? () => props.onClick() : '' }
        > {props.value && props.value} {props.icon && props.icon}</button> 
    )
}

CustomButton.propTypes = {
    type: PropTypes.string,
    style: PropTypes.shape({}),
    value: PropTypes.string,
    btnStyleType: PropTypes.string.isRequired,
    icon: PropTypes.shape({}),
    onClick: PropTypes.func,
}

CustomButton.defaultProps = {
    value: null,
    focus: false,
    type: 'submit',
    placeholder: null,
    style: null,
    btnStyleType: "primary primaryBtn",
    icon: null,
    onClick: null
}

export default CustomButton;